package com.example.demo.service;


import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {
	@Autowired
	ReportRepository reportRepository;

	// レコード全件取得
	public List<Report> findAllReport(String start, String end) {

		if(!start.isBlank()) {
			start += " 00:00:00";
		}else {
			start = "2022-01-01 00:00:00";
		}

		if(!end.isBlank()) {
			end += " 23:59:59";
		}else {
			end = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		}

        Timestamp startDate = Timestamp.valueOf(start);
    	Timestamp endDate = Timestamp.valueOf(end);

		return reportRepository.findbyOrdertBycreated_Date(startDate, endDate);
	}

	// レコード追加
	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	// レコード削除
	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}

	// レコード1件取得
	public Report editReport(Integer id) {
		Report report = (Report) reportRepository.findById(id).orElse(null);
		return report;
	}

}
