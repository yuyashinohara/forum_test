package com.example.demo.repository;


import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Report;

@Repository
public interface ReportRepository extends JpaRepository<Report, Integer> {
	@Query("select u from Report u where u.created_Date between :startDate and :endDate order by updated_Date desc")
	List<Report> findbyOrdertBycreated_Date(@Param("startDate") Timestamp startDate, @Param("endDate") Timestamp endDate);

}
